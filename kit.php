<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>

        <div class="divider"></div>

        <div class="figure figure-yellow">
            <div class="container">
                <div class="figure-row">
                    <div class="figure-heading">
                        <span>Цифра дня</span>
                        <strong>1039 книг</strong>
                    </div>
                    <div class="figure-content">Помогли собрать активисты МИЦ Минестерству культуры для создания книжных точек в больницах области.</div>
                </div>
            </div>
        </div>

        <section class="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-lg-8">
                        <div class="block-long mb-30">
                            <div class="block-long-row">
                                <div class="block-long-left">
                                    <div class="h1">Чего можешь достичь?</div>
                                    <div class="text">Вступи в МИЦ и узнай!</div>
                                </div>
                                <div class="block-long-right">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Узнать подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4">
                        <div class="block-high mb-30">
                            <div class="block-high-inner">
                                <div class="h1">Вам есть о чем рассказать?</div>
                                <div class="block-subtitle">Мы готовы разместить вашу новость на нашем портале!</div>
                                <div class="block-text">Для этого вы можете написать нажав по кнопке ниже, "предложить новость" и после прохождения модерации она появится на нашем сайте, также вы сможете стать автором и попасть в Informat</div>
                                <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить новость</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="content">

                    <div class="row">
                        <div class="col-sm-12 col-sm-6 col-md-4">
                            <h1>Заголовок Н1</h1>
                            <h2>Заголовок Н2</h2>
                            <h3>Заголовок Н3</h3>
                            <h4>Заголовок Н4</h4>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Инпут обычный">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" placeholder="Инпут обычный">
                            </div>
                            <div class="form-group">
                                <select name="select" class="form-select">
                                    <option value="Выпадающий список">Выпадающий список</option>
                                    <option value="Пункт меню списка">Пункт меню списка</option>
                                    <option value="Пункт меню списка">Пункт меню списка</option>
                                    <option value="Пункт меню списка">Пункт меню списка</option>
                                    <option value="Пункт меню списка">Пункт меню списка</option>
                                    <option value="Пункт меню списка">Пункт меню списка</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control-date datepicker-here" placeholder="Календарь">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="Ваш комментарйи к статье..." rows="4"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="radio">
                                            <input type="radio" name="radio" class="radio-input" value="1" checked>
                                            <span class="radio-text">Радиобаттон</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="radio">
                                            <input type="radio" name="radio" class="radio-input" value="2">
                                            <span class="radio-text">Радиобаттон</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" name="checkbox" class="checkbox-input" value="1" checked>
                                            <span class="checkbox-text">Чекбокс</span>
                                        </label>
                                    </div>
                                    <div class="form-checkbox">
                                        <label class="checkbox">
                                            <input type="checkbox" name="checkbox" class="checkbox-input" value="2">
                                            <span class="checkbox-text">Чекбокс</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <button type="submit" class="btn">Отправить</button>
                        </div>
                    </div>

                    <div class="mb-30"></div>

                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <ol>
                                <li>Список нумерованный</li>
                                <li>Список нумерованный</li>
                                <li>Список нумерованный</li>
                                <li>Список нумерованный</li>
                                <li>Список нумерованный</li>
                            </ol>
                        </div>
                        <div class="col-sm-4 col-md-3">
                            <ul>
                                <li>Список маркерованный</li>
                                <li>Список маркерованный</li>
                                <li>Список маркерованный</li>
                                <li>Список маркерованный</li>
                                <li>Список маркерованный</li>
                            </ul>
                        </div>
                    </div>

                    <div class="mb-30"></div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">

                            <div class="table-responsive">

                                <table class="table">
                                    <tr>
                                        <th>Таблица</th>
                                        <th>Колонка 1</th>
                                        <th>Колонка 2</th>
                                    </tr>
                                    <tr>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                    </tr>
                                    <tr>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                    </tr>
                                    <tr>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                    </tr>
                                    <tr>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                        <td>Пункт таблицы</td>
                                    </tr>
                                </table>

                            </div>

                            <div class="mb-30"></div>

                            <a href="#" class="btn-pdf"><span>Мы открываем новую площадку, на которой молодежь</span></a>

                            <div class="mb-30"></div>

                            <p>"Время быть лидером" новый проект от Чердаклинского отделения. <i>Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские навыки</i>, познакомиться с новыми людьми и узнать то, что раньше было покрыто занавесом тайн.</p>
                            <p>Ждем к себе в гости активных, амбициозных и любопытных! Если ты хочешь развить <strong>лидерские качества и научиться работать в команде</strong>, то тебе к нам! <u>По всем вопросам обращайтесь</u> к https://ulmic.ru/671 (Виктории Тарасовой) Следи за новостями на нашей страничке: https://vk.com/event154326865</p>

                            <div class="mb-30"></div>

                            <blockquote>Благодарственное письмо Губернатора Ульяновской области выдано за активное участие в реализации социально значимых проектов на территории Ульяновской области, за достигнутые успехи и плодотворную общественную деятельность.</blockquote>

                            <div class="mb-30"></div>

                            <div class="text-lead">Мероприятие прошло на территории Ульяновского мебельного комбината. Участие в нём приняли порядка 30 человек – представители власти и общественных организаций. МИЦ представлял первый заместитель председателя Павел Андреев.</div>
                        </div>
                    </div>

                    <div class="mb-30"></div>

                    <h1 class="text-center">Губернатор поблагодарил МИЦ за достигнутые успехи</h1>
                    <img src="images/article-image.jpg" class="img-responsive" alt="">
                    <br/>
                    <p>Ассоциация начинающих журналистов Ульяновской области «Inформат» (структурное подразделение МИЦ) совместно с Министерством молодёжного развития Ульяновской области, Управлением информационной политики администрации губернатора Ульяновской области формирует реестр «Малые медиа Ульяновской области»</p>
                    <p>Включённые в реестр редакции получат возможность направлять свои делегации на интеллект-лагерь молодых журналистов Ульяновской области (декабрь 2017 г.), Межрегиональный фестиваль молодых журналистов Ульяновской области (май 2018 г.), участвовать в грантовом конкурсе для малых медиа региона, конкурсе начинающих журналистов Ульяновской области «Новый взгляд» и мн.др.</p>

                    <div class="image image-left">
                        <img src="images/article-image_sm.jpg" class="img-responsive" alt="">
                        <i> Название изображения </i>
                    </div>
                    <p>Включённые в реестр редакции получат возможность направлять свои делегации на интеллект-лагерь молодых журналистов Ульяновской области (декабрь 2017 г.), Межрегиональный фестиваль молодых журналистов Ульяновской области (май 2018 г.), участвовать в грантовом конкурсе для малых медиа региона, конкурсе начинающих журналистов Ульяновской области «Новый взгляд» и мн.др.</p>
                    <p>Включённые в реестр редакции получат возможность направлять свои делегации на интеллект-лагерь молодых журналистов Ульяновской области (декабрь 2017 г.), Межрегиональный фестиваль молодых журналистов Ульяновской области (май 2018 г.), участвовать в грантовом конкурсе для малых медиа региона, конкурсе начинающих журналистов Ульяновской области «Новый взгляд» и мн.др. Ассоциация начинающих журналистов Ульяновской области «Inформат» (структурное подразделение МИЦ) совместно с Министерством молодёжного развития Ульяновской области, Управлением информационной политики администрации губернатора Ульяновской области формирует реестр «Малые медиа Ульяновской области».</p>

                    <br/>

                    <div class="video-block">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://www.youtube.com/embed/8rIcwfXIPF8" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="video-cast">
                            <div class="video-cast-social">
                                <span>Рассказать друзьям:</span>
                                <a href="#"><i class="fa fa-vk"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                            <div class="video-cast-button">
                                <a href="#" class="btn">Я хочу учавствовать</a>
                            </div>
                        </div>
                    </div>

                    <div class="mb-30"></div>

                    <div class="row">
                        <div class="col-md-8 col-lg-8">
                            <div class="slider">
                                <div class="slider-main">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                </div>
                                <div class="slider-thumbs">
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                </div>
                            </div>
                            <div class="slider-author">
                                <div class="author-photo">
                                    <img src="images/author_photo.png" class="img-responsive" alt="">
                                </div>
                                <div class="author-text">
                                    Фотограф:
                                    <strong>Иванов Иван Алексеевич</strong>
                                </div>
                                <div class="social-list">
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="mb-30"></div>


                <div class="contact">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="h2 heading-arrow">Адрес Президиума МИЦ</div>
                            <div class="contact-address">Россия, 432071, г.Ульяновск, ул. Кролюницкого, 15а</div>
                            <div class="contact-item">
                                <a class="tel" href="tel:+79656965303"><i class="fa fa-phone"></i> <span>+7 (965) 696-53-03</span></a>
                                <br/>
                                <a class="email" href="mailto:fomina@ulmic.ru"><i class="fa fa-envelope"></i> <span>fomina@ulmic.ru</span></a>
                            </div>
                            <h4>Мы в соцсетях:</h4>
                            <div class="social-list">
                                <a href="#"><i class="fa fa-vk"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div id="map" class="map"></div>
                        </div>
                    </div>
                </div>

                <div class="mb-30"></div>

                <div class="content">

                    <div class="h1">Фильтр материалов</div>

                    <div class="row">
                        <div class="col-md-7 col-lg-6">

                            <div class="filter-tags">
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>Гражданская активность</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>Журналистика</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>IT</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>Право</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>Добровольчество</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>НКО</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'">
                                        <span>Корпоративные проекты</span>
                                    </label>
                                </div>
                                <div class="tags-item">
                                    <label>
                                        <input type="checkbox" value="'" checked>
                                        <span>Все новости</span>
                                    </label>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="mb-30"></div>

                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <div class="h2 heading-arrow">Оставьте свой комментарий</div>
                        <div class="comments">

                            <div class="comments-answer">
                                <div class="comments-row">
                                    <div class="comments-col-left">
                                        <div class="comments-avatar">
                                            <img src="images/author_photo.png" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="comments-col-right">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" placeholder="Ваш комментарйи к статье..." rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn">Оставить комментарий</button>
                                </div>
                            </div>

                            <div class="post">
                                <div class="comments-row">
                                    <div class="comments-col-left">
                                        <div class="comments-avatar">
                                            <img src="images/author_photo.png" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                    <div class="comments-col-right">
                                        <div class="post-heading">
                                            <span class="post-author">Валерий Иванов</span>
                                            <span class="post-date">19 марта 2017 в 12:45</span>
                                        </div>
                                        <div class="post-text">Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. У Молодёжного инициативного центра всероссийский экспертный совет поддержал проект «Вместе и учась друг у друга». В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</div>
                                        <a href="#" class="post-like"><i class="fa fa-heart-o" aria-hidden="true"></i> <span>387</span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-30"></div>

                <div class="news-block">
                    <div class="news-block-image">
                        <div class="image-wrap">
                            <div class="image-inner">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="news-block-content">
                        <div class="hews-block-tags">29 окт. 2017 12:00 - 15:00  |  Автор: <a href="#">Марина кузнецова</a></div>
                        <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a> / <a href="#" class="news-article">Право</a></div>
                        <div class="news-block-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую. "Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую. "Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую.</div>
                        <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                    </div>
                </div>


                <div class="mb-60"></div>

                <div class="h2 heading-arrow">Карточка контактов</div>

                <div class="row">

                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="contact-card">
                            <div class="contact-card-image">
                                <div class="card-image-inner">
                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="contact-card-content">
                                <div class="contact-card-header">
                                    <div class="contact-name">Константин Ильин</div>
                                    <div class="contact-staff">Председатель</div>
                                    <div class="contact-source">
                                        <a class="tel" href="tel:+79656965303"><i class="fa fa-phone"></i> <span>+7 (965) 696-53-03</span></a>
                                        <br/>
                                        <a class="email" href="mailto:fomina@ulmic.ru"><i class="fa fa-envelope"></i> <span>fomina@ulmic.ru</span></a>
                                    </div>
                                    <div class="social-list">
                                        <a href="#"><i class="fa fa-vk"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                        <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                        <a href="#"><i class="fa fa-youtube"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="contact-card">
                            <div class="contact-card-image">
                                <div class="card-image-inner">
                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                            <div class="contact-card-content">
                                <div class="contact-card-header">
                                    <div class="contact-name">Константин Ильин</div>
                                    <div class="contact-staff">Председатель</div>
                                    <div class="contact-source">
                                        <a class="tel" href="tel:+79656965303"><i class="fa fa-phone"></i> <span>+7 (965) 696-53-03</span></a>
                                        <br/>
                                        <a class="email" href="mailto:fomina@ulmic.ru"><i class="fa fa-envelope"></i> <span>fomina@ulmic.ru</span></a>
                                    </div>
                                    <div class="social-list">
                                        <a href="#"><i class="fa fa-vk"></i></a>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                        <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                        <a href="#"><i class="fa fa-youtube"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="mb-60"></div>

                <div class="h1 heading-arrow">Предложите вашу новость!</div>

                <div class="row">
                    <div class="col-md-7 col-lg-8">
                        <div class="news-add">
                            <div class="form-group">
                                <textarea class="form-control" name="message" placeholder="" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="Заголовок новости">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group pt-5">
                                        <select class="form-select" name="select">
                                            <option value="Выберете категорию">Выберете категорию</option>
                                            <option value="Категория 1">Категория 1</option>
                                            <option value="Категория 2">Категория 2</option>
                                            <option value="Категория 3">Категория 3</option>
                                            <option value="Категория 4">Категория 4</option>
                                            <option value="Категория 5">Категория 5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn"><i class="fa fa-paper-plane"></i> Отправить новость</button>
                                </div>
                            </div>
                        </div>

                        <div class="mb-60"></div>

                        <ul class="pagination">
                            <li class="prev"><a href="#"></a></li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li class="next"><a href="#"></a></li>
                        </ul>

                    </div>
                    <div class="col-md-5 col-lg-4">
                        <div class="column-gray">
                            <div class="h3 heading-arrow">Коментарии редактора</div>
                            <div class="column-date">13.10.17 от Михаила</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
                            <div class="column-divider"></div>
                            <p><strong>Ваш вопрос редактору?</strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
                            <form class="form">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" placeholder="Ваш вопрос редактору" rows="4"></textarea>
                                </div>
                               <div class="text-center">
                                   <button type="submit" class="btn"><i class="fa fa-paper-plane"></i> Задать вопрос</button>
                               </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="mb-60"></div>

                <h2 class="heading-arrow">Лента событий</h2>

                <div class="events">

                    <div class="events-item">
                        <div class="events-left">
                            <div class="events-image">
                                <img src="images/user_photo.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="events-right">
                            <div class="events-header"><strong>Валерий Иванов </strong> <span>19 марта 2017 в 12:45</span></div>
                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>
                        </div>
                    </div>

                    <div class="events-item">
                        <div class="events-left">
                            <div class="events-image">
                                <img src="images/user_photo.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="events-right">
                            <div class="events-header"><strong>Валерий Иванов </strong> <span>19 марта 2017 в 12:45</span></div>
                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>
                        </div>
                    </div>

                    <div class="events-item">
                        <div class="events-left">
                            <div class="events-image">
                                <img src="images/user_photo.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="events-right">
                            <div class="events-header"><strong>Валерий Иванов </strong> <span>19 марта 2017 в 12:45</span></div>
                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>
                        </div>
                    </div>

                    <div class="events-item">
                        <div class="events-left">
                            <div class="events-image">
                                <img src="images/user_photo.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="events-right">
                            <div class="events-header"><strong>Валерий Иванов </strong> <span>19 марта 2017 в 12:45</span></div>
                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>
                        </div>
                    </div>

                    <div class="events-item">
                        <div class="events-left">
                            <div class="events-image">
                                <img src="images/user_photo.jpg" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="events-right">
                            <div class="events-header"><strong>Валерий Иванов </strong> <span>19 марта 2017 в 12:45</span></div>
                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>
                        </div>
                    </div>


                </div>

                <div class="mb-60"></div>

                <div class="content">
                    <div class="h1">Ораторское искуство</div>
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="images/news_image.jpg" class="img-responsive mb-20">
                            <div class="text-center mb-30">
                                <a href="#" class="btn">Мероприятия</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="h3">Популярные материалы</div>
                            <ul>
                                <li><a href="#">Заголовок статьи</a> - <span>22.11.17 в 15:30</span></li>
                                <li><a href="#">Заголовок статьи</a> - <span>22.11.17 в 15:30</span></li>
                                <li><a href="#">Заголовок статьи</a> - <span>22.11.17 в 15:30</span></li>
                                <li><a href="#">Заголовок статьи</a> - <span>22.11.17 в 15:30</span></li>
                                <li><a href="#">Заголовок статьи</a> - <span>22.11.17 в 15:30</span></li>
                            </ul>
                            <a class="btn-view" href="#">Все материалы</a>
                        </div>
                    </div>
                </div>
                <div class="mb-60"></div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>

        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                        center: [55.76, 37.64],
                        zoom: 10
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    // Создание макета содержимого хинта.
                    // Макет создается через фабрику макетов с помощью текстового шаблона.
                    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
                        "<b>{{ properties.object }}</b><br />" +
                        "{{ properties.address }}" +
                        "</div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );

                var myPlacemark = new ymaps.Placemark([55.764286, 37.581408], {
                    address: "Москва, ул. Зоологическая, 13, стр. 2",
                    object: "Центр современного искусства"
                }, {
                    hintLayout: HintLayout
                });

                myMap.geoObjects.add(myPlacemark);
            }

        </script>

    </body>
</html>

