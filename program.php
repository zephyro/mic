<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>

        <section class="page">


            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="#">Лидерство</a></li>
                    <li>«Лидер»</li>
                </ul>

                <div class="rows">

                    <div class="col-left">

                        <div class="page-body content">

                            <div class="heading-yellow">
                                <h1 class="heading-arrow-white">Подробнее в видеорепортаже</h1>
                            </div>

                            <img src="images/program_01.jpg" class="img-responsive" alt="">

                            <div class="slider-author mb-30">
                                <div class="author-photo">
                                    <img src="images/author_photo.png" class="img-responsive" alt="">
                                </div>
                                <div class="author-text">
                                    Фотограф:
                                    <strong>Иванов Иван Алексеевич</strong>
                                </div>
                                <div class="social-list">
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                                <div class="meta">
                                    <span class="meta-item"><i class="fa fa-eye"></i> <strong>1350</strong></span>
                                    <span class="meta-item"><i class="fa fa-share-alt"></i> <strong>695</strong></span>
                                </div>
                            </div>

                            <p>Известно, что лидерское поведение крайне важно для успешной профессиональной и личной жизни. Если мы способны достойно заявить о себе и вести людей за собой, значит, наша позиция будет выигрышной в самых разных ситуациях – от сдачи экзамена до прохождения собеседования при приёме на работу, от дружеской вечеринки до построения личных отношений.</p>

                            <blockquote>Благодарственное письмо Губернатора Ульяновской области выдано за активное участие в реализации социально значимых проектов на территории Ульяновской области, за достигнутые успехи и плодотворную</blockquote>

                            <h2 class="heading-arrow">ЧТО ВХОДИТ?</h2>

                            <p>Мероприятия программы постоянно дополняются, изменяются исходя из развивающихся современных подходов к неформальному образованию, потребностей общества и самой молодёжи. Однако неизменными остаётся концепция программы. Согласно ей центральная программа каждый год будет включать следующее:</p>

                            <ul class="list-arrow">
                                <li>работу стационарных объединений неформального образования (лидер-клубы МИЦ, Клубы гражданской активности и пр.), чтобы каждый мог ,без выезда из своего района, повысить собственные компетенции;</li>
                                <li>личностный образовательно-конкурсный проект. В нашем случае это конкурса «Я-лидер!», пропитанный одновременно и духом соревнования, в тоже время включающий и все этапы развития качеств, необходимых современно лидеру, управленцу и организатору – от получения новый знаний и моделей поведения до условий в которых молодой человек может применить их на практике;</li>
                                <li>работа с тренерами, приглашёнными из других регионов и стран. Мы против того, чтобы «вариться в собственном соку». Молодой человек должен быть в теме последних трендов, современных технологий и подходов. Именно поэтому ежегодно мы устраиваем мероприятия с приглашением сторонних экспертов;</li>
                                <li>краткосрочные выездные мероприятия;</li>
                                <li>методическое сопровождение.</li>
                            </ul>

                            <br/>

                            <h2 class="heading-arrow">МЕРОПРИЯТИЯ ПРОГРАММЫ</h2>

                            <div class="event-box">
                                <div class="event-box-image">
                                    <div class="eb-image-inner">
                                        <img src="images/eb-image.jpg" class="img-responsive" alt="">
                                    </div>

                                    <div class="event-box-text">
                                        <div class="h1">КЛУБЫ ГРАЖДАНСКОЙ АКТИВНОСТИ</div>
                                        <div class="eb-text">Ближайшее мероприятие: 18.10.17</div>
                                    </div>
                                </div>
                                <div class="event-box-content">
                                    <p>Это сеть, развёрнутая в различных муниципальных образованиях области на базе местных отделений МИЦ. Встречи Клуба проходят систематически. В них разбираются самые различные темы: от того кто такой "лидер", как сформировать эффективную команду до ораторского искусства, конфликтологии, организации и проведения собственных мероприятий. Точное расписание встреч с радостью сообщат в наших отделениях.</p>
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Предложить новость</a>
                                </div>
                            </div>

                            <div class="event-box">
                                <div class="event-box-image">
                                    <div class="eb-image-inner">
                                        <img src="images/eb-image2.jpg" class="img-responsive" alt="">
                                    </div>

                                    <div class="event-box-text">
                                        <div class="h1">я- лидер</div>
                                        <div class="eb-text">Ближайшее мероприятие: 20.10.17</div>
                                    </div>
                                </div>
                                <div class="event-box-content">
                                    <p>Это сеть, развёрнутая в различных муниципальных образованиях области на базе местных отделений МИЦ. Встречи Клуба проходят систематически. В них разбираются самые различные темы: от того кто такой "лидер", как сформировать эффективную команду до ораторского искусства, конфликтологии, организации и проведения собственных мероприятий. Точное расписание встреч с радостью сообщат в наших отделениях.</p>
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Предложить новость</a>
                                </div>
                            </div>

                            <div class="event-box">
                                <div class="event-box-image">
                                    <div class="eb-image-inner">
                                        <img src="images/eb-image3.jpg" class="img-responsive" alt="">
                                    </div>

                                    <div class="event-box-text">
                                        <div class="h1">Активатор</div>
                                        <div class="eb-text">Ближайшее мероприятие: 20.10.17</div>
                                    </div>
                                </div>
                                <div class="event-box-content">
                                    <p>Это сеть, развёрнутая в различных муниципальных образованиях области на базе местных отделений МИЦ. Встречи Клуба проходят систематически. В них разбираются самые различные темы: от того кто такой "лидер", как сформировать эффективную команду до ораторского искусства, конфликтологии, организации и проведения собственных мероприятий. Точное расписание встреч с радостью сообщат в наших отделениях.</p>
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Предложить новость</a>
                                </div>
                            </div>

                            <h2 class="heading-arrow">Прежние руководители:</h2>
                            <div class="user-box">
                                <div class="user-box-image">
                                    <div class="user-box-image-item">
                                        <img src="images/card_image.jpg" class="img-responsive" alt="">
                                    </div>
                                </div>
                                <div class="user-box-content">
                                    <div class="user-box-name">Константин Ильин</div>
                                    <div class="user-box-staff"><strong>Период работы</strong>: 20.02.2007г. - 21.14.2010г.</div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-right">

                        <div class="side-block-shadow mb-30">

                            <div class="side-box">

                                <div class="h3 heading-arrow">новости Программы «лидер»</div>

                                <ul class="side-news">
                                    <li>
                                        <div class="side-news-row">
                                            <div class="side-news-image">
                                                <img src="images/news_image.jpg" class="img-responsive">
                                            </div>
                                            <div class="side-news-text">
                                                <div class="side-news-date">12 октября 2017</div>
                                                <div class="side-news-name"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-news-date">12 октября 2017</div>
                                        <div class="side-news-name"><a href="#">Экспедиция МИЦ будет длиться 27 дней</a></div>
                                    </li>
                                    <li>
                                        <div class="side-news-date">12 октября 2017</div>
                                        <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="side-box">

                                <div class="h3 heading-arrow">мероприятия</div>
                                <ul class="event-nav">
                                    <li><a href="#">Прошедшие</a></li>
                                    <li><a href="#">грядущие</a></li>
                                </ul>

                                <div class="side-event">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                    <div class="side-event-heading"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                    <div class="side-event-date">12 октября 2017</div>
                                    <a href="#" class="btn" tabindex="0"><i class="fa fa-eye"></i> Узнать подробнее</a>
                                </div>

                            </div>

                            <div class="side-box-wrap">
                                <div class="block-high block-high-yellow">
                                    <div class="block-high-inner">
                                        <div class="h1">новостная подписка</div>
                                        <div class="block-subtitle no-line">Подпишись на нашиновости прямо сейчас:</div>
                                        <form class="form">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Ваш e-mail">
                                            </div>
                                            <button class="btn btn-blue"><i class="fa fa-paper-plane"></i> Предложить новость</button>
                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="side-box side-block-shadow">

                            <div class="h3 heading-arrow">новости</div>

                            <ul class="side-news">
                                <li>
                                    <div class="side-news-row">
                                        <div class="side-news-image">
                                            <img src="images/news_image.jpg" class="img-responsive">
                                        </div>
                                        <div class="side-news-text">
                                            <div class="side-news-date">12 октября 2017</div>
                                            <div class="side-news-name"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Экспедиция МИЦ будет длиться 27 дней</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                </li>
                            </ul>
                        </div>

                        <div class="block-high mb-30">
                            <div class="block-high-inner">
                                <div class="h1">Вам есть о чем рассказать?</div>
                                <div class="block-subtitle">Мы готовы разместить вашу новость на нашем портале!</div>
                                <div class="block-text">Для этого вы можете написать нажав по кнопке ниже, "предложить новость" и после прохождения модерации она появится на нашем сайте, также вы сможете стать автором и попасть в Informat</div>
                                <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить новость</a>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>

        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                        center: [55.76, 37.64],
                        zoom: 10
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    // Создание макета содержимого хинта.
                    // Макет создается через фабрику макетов с помощью текстового шаблона.
                    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
                        "<b>{{ properties.object }}</b><br />" +
                        "{{ properties.address }}" +
                        "</div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );

                var myPlacemark = new ymaps.Placemark([55.764286, 37.581408], {
                    address: "Москва, ул. Зоологическая, 13, стр. 2",
                    object: "Центр современного искусства"
                }, {
                    hintLayout: HintLayout
                });

                myMap.geoObjects.add(myPlacemark);
            }

        </script>

    </body>
</html>

