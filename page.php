<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>

        <section class="page">
            <div class="container">
                <ul class="breadcrumbs">
                    <li>Новости</li>
                </ul>
                <div class="rows">
                    <div class="col-left">
                        <div class="page-body content">
                            <img src="images/jumbotron_01.jpg" class="img-responsive" alt="">
                            <div class="slider-author mb-30">
                                <div class="author-photo">
                                    <img src="images/author_photo.png" class="img-responsive" alt="">
                                </div>
                                <div class="author-text">
                                    Фотограф:
                                    <strong>Иванов Иван Алексеевич</strong>
                                </div>
                                <div class="social-list">
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                                <div class="meta">
                                    <span class="meta-item"><i class="fa fa-eye"></i> <strong>1350</strong></span>
                                    <span class="meta-item"><i class="fa fa-share-alt"></i> <strong>695</strong></span>
                                </div>
                            </div>

                            <h1 class="heading-h1">Губернатор поблагодарил МИЦ за достигнутые успехи</h1>

                            <div class="text-border">Морозов вручил благодарственное письмо на имя организации сегодня встречаясь с представителями некоммерческих организаций региона.</div>

                            <div class="text-lead">Мероприятие прошло на территории Ульяновского мебельного комбината. Участие в нём приняли порядка 30 человек – представители власти и общественных организаций. МИЦ представлял первый заместитель председателя Павел Андреев.</div>

                            <p>Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. У Молодёжного инициативного центра всероссийский экспертный совет поддержал проект «Вместе и учась друг у друга». В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</p>

                            <blockquote>Благодарственное письмо Губернатора Ульяновской области выдано за активное участие в реализации социально значимых проектов на территории Ульяновской области, за достигнутые успехи и плодотворную</blockquote>

                            <p>Сергей Морозов вручил благодарственное письмо на имя организации сегодня встречаясь с представителями некоммерческих организаций региона.</p>

                            <p>Мероприятие прошло на территории Ульяновского мебельного комбината. Участие в нём приняли порядка 30 человек – представители власти и общественных организаций. МИЦ представлял первый заместитель председателя Павел Андреев.</p>

                            <h2 class="heading-arrow">Подробнее в видеорепортаже</h2>

                            <div class="video-block">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://www.youtube.com/embed/8rIcwfXIPF8" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>

                            <h2 class="heading-arrow">Фотоотчёт с места событий</h2>

                            <div class="slider">

                                <div class="slider-main">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                </div>

                                <div class="slider-thumbs">
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                </div>

                            </div>

                            <div class="slider-author ">
                                <div class="author-photo">
                                    <img src="images/author_photo.png" class="img-responsive" alt="">
                                </div>
                                <div class="author-text">
                                    Фотограф:
                                    <strong>Иванов Иван Алексеевич</strong>
                                </div>
                                <div class="social-list">
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>

                            <div class="article-info">
                                <div class="active-row">
                                    <div class="article-column">
                                        <div class="article-info-heading">Упомянутые в статье:</div>
                                        <div class="article-people">
                                            <a href="#">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </a>
                                            <a href="#">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </a>
                                            <a href="#">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </a>
                                            <a href="#">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </a>
                                            <a href="#">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="article-column">
                                        <div class="article-info-heading">Теги к статье:</div>
                                        <div class="article-tags">
                                            <a href="#">НАГРАДа</a>
                                            <a href="#">Политика</a>
                                            <a href="#">Дипломат</a>
                                            <a href="#">Депутат</a>
                                            <a href="#">МИЦ</a>
                                            <a href="#">статьи</a>
                                            <a href="#">Ресурс</a>
                                            <a href="#">город</a>
                                            <a href="#">регион</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="promo">
                                <div class="promo-text">
                                    <div class="promo-text">
                                        <span>Хотите стать<br/>членом МИЦ?</span>
                                        <strong>Вступайте<br/>прямо сейчас</strong>
                                    </div>
                                    <a href="#" class="btn-idea">
                                        <svg class="ico-svg" viewBox="0 0 360 60" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite.svg#icon-bg" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                        <span><i class="fa fa-paper-plane"></i>Отправить заявку</span>
                                    </a>
                                </div>
                            </div>

                            <div class="h2 heading-arrow">Оставьте свой комментарий</div>
                            <div class="comments">

                                <div class="comments-answer">
                                    <div class="comments-row">
                                        <div class="comments-col-left">
                                            <div class="comments-avatar">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="comments-col-right">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" placeholder="Ваш комментарйи к статье..." rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn">Оставить комментарий</button>
                                    </div>
                                </div>

                                <div class="post">
                                    <div class="comments-row">
                                        <div class="comments-col-left">
                                            <div class="comments-avatar">
                                                <img src="images/author_photo.png" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="comments-col-right">
                                            <div class="post-heading">
                                                <span class="post-author">Валерий Иванов</span>
                                                <span class="post-date">19 марта 2017 в 12:45</span>
                                            </div>
                                            <div class="post-text">Открывая встречу Сергей Морозов вручил благодарственные письма и дипломы победителям первого конкурса Фонда-оператора президентских грантов. У Молодёжного инициативного центра всероссийский экспертный совет поддержал проект «Вместе и учась друг у друга». В ближайшие дни в рамках него будет открыто первое молодёжное образовательное пространство для работы инициативной молодёжи «Окна».</div>
                                            <a href="#" class="post-like"><i class="fa fa-heart-o" aria-hidden="true"></i> <span>387</span></a>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-right">
                        <div class="side-box box-gray">
                            <div class="h3 heading-arrow">Лента новостей</div>
                            <ul class="side-news">
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Экспедиция МИЦ будет длиться 27 дней</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                </li>
                            </ul>
                        </div>

                        <div class="side-box box-white">
                            <div class="h3 heading-arrow">новости Программы «лидер»</div>
                            <ul class="side-news">
                                <li>
                                    <div class="side-news-row">
                                        <div class="side-news-image">
                                            <img src="images/news_image.jpg" class="img-responsive">
                                        </div>
                                        <div class="side-news-text">
                                            <div class="side-news-date">12 октября 2017</div>
                                            <div class="side-news-name"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Экспедиция МИЦ будет длиться 27 дней</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                </li>
                            </ul>
                        </div>

                        <div class="side-box box-gray">
                            <div class="h3 heading-arrow">Похожие мероприятия</div>
                            <div class="side-event">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                                <div class="side-event-heading"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                <div class="side-event-date">12 октября 2017</div>
                                <a href="#" class="btn" tabindex="0"><i class="fa fa-eye"></i> Узнать подробнее</a>
                            </div>
                        </div>

                        <div class="block-high mb-30">
                            <div class="block-high-inner">
                                <div class="h1">Вам есть о чем рассказать?</div>
                                <div class="block-subtitle">Мы готовы разместить вашу новость на нашем портале!</div>
                                <div class="block-text">Для этого вы можете написать нажав по кнопке ниже, "предложить новость" и после прохождения модерации она появится на нашем сайте, также вы сможете стать автором и попасть в Informat</div>
                                <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить новость</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>

        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                        center: [55.76, 37.64],
                        zoom: 10
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    // Создание макета содержимого хинта.
                    // Макет создается через фабрику макетов с помощью текстового шаблона.
                    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
                        "<b>{{ properties.object }}</b><br />" +
                        "{{ properties.address }}" +
                        "</div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );

                var myPlacemark = new ymaps.Placemark([55.764286, 37.581408], {
                    address: "Москва, ул. Зоологическая, 13, стр. 2",
                    object: "Центр современного искусства"
                }, {
                    hintLayout: HintLayout
                });

                myMap.geoObjects.add(myPlacemark);
            }

        </script>

    </body>
</html>

