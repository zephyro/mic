// Header Menu

$(function() {
    var box = $('.topnav');

    $('.navbar-toggle').on('click', function(e) {
        e.preventDefault();
        box.removeClass('open-search')
        box.toggleClass('open-nav');
    });

    $('.header-search-btn, .navbar-search').on('click', function(e) {
        e.preventDefault();
        box.removeClass('open-nav');
        box.toggleClass('open-search');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 992 ) {
            box.removeClass('open-nav');
        }
    });

    $('body').click(function (event) {
        if(($(event.target).closest(box).length === 0)) {
            box.removeClass('open-nav');
            box.removeClass('open-search');
        }
    });
});


$('.jumbotron-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false
            }
        }
    ]
});


$('.press-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 10000,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1
            }
        }
    ]
});

// Forms Style

$('.form-select').selectric({
    disableOnMobile: false,
    responsive: true,
    maxHeight: 350
});


$('.slider-main').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"></span>',
    nextArrow: '<span class="slide-nav next"></span>',
    asNavFor: '.slider-thumbs',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false
            }
        }
    ]
});

$('.slider-thumbs').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    autoplay: true,
    focusOnSelect: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-main'
});


$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        if ($('.btn-up').is(':hidden')) {
            $('.btn-up').css({opacity : 1}).fadeIn('slow');
        }
    } else { $('.btn-up').stop(true, false).fadeOut('fast'); }
});
$('.btn-up').click(function() {
    $('html, body').stop().animate({scrollTop : 0}, 300);
});



$('.tabs-nav li a').on('click touchstart', function(e){
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
});


$('.materials-nav li a').on('click touchstart', function(e){
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.tabs');

    $(this).closest('.materials-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$('.operation-header').on('click touchstart', function(e){
    e.preventDefault();
    var box = $(this).closest('.operation-item');

    $('.operation-accordion').find('.operation-item').removeClass('open');
    box.addClass('open');
});
