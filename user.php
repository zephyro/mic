<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>

        <section class="page">


            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="#">Главная</a></li>
                    <li>Чернета Наталья Владимировна</li>
                </ul>
                
                <div class="user-card">
                    <div class="user-row">

                        <div class="user-left">
                            <div class="user-image">
                                <div class="user-image-item">
                                    <img src="images/user_profile_photo.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="user-ticket">Членский билет №746</div>
                            </div>

                            <div class="social-list">
                                <a href="#"><i class="fa fa-vk"></i></a>
                                <a href="#"><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>

                        <div class="user-right">
                            <div class="user-heading">
                                <h1>Чернета Наталья Владимировна</h1>
                            </div>
                            <p>Куратор центральной программы «Информация» в местном отделении в г. Димитровграде Пресс-секретарь местного отделения в г. Димитровграде</p>

                            <blockquote>Благодарственное письмо Губернатора Ульяновской области выдано за активное участие в реализации социально значимых проектов на территории Ульяновской области, за достигнутые успехи и плодотворную общественную деятельность.</blockquote>

                            <h2 class="heading-arrow">Команда:</h2>

                            <ul class="list">
                                <li>Местное отделение в г. Ульяновске</li>
                                <li>Ассоциация начинающих журналистов «Inформат»</li>
                            </ul>

                            <div class="user-list">

                                <div class="user-godfather">
                                    <div class="user-title">Крестный:</div>
                                    <a href="#" class="people-item">
                                        <div class="people-avatar">
                                            <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="people-name">Валерий Фролов</div>
                                    </a>
                                </div>

                                <div class="user-godson">
                                    <div class="user-title">Крестники:</div>
                                    <div class="godson-list">
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                        <a href="#" class="godson">
                                            <div class="godson-image">
                                                <img src="images/user_photo.jpg" class="img-responsive" alt="">
                                            </div>
                                            <div class="godson-name">Валерий<br/>Фролов</div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        <div class="user-advantage">
            <div class="container">

                <h2 class="heading-arrow">Команда:</h2>

                <ul>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_01.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_02.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_03.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_04.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_05.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_06.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                    <li>
                        <div class="user-advantage-image">
                            <img src="img/advant_icon_07.png" class="img-responsive" alt="">
                        </div>
                        <span>2017</span>
                    </li>
                </ul>
            </div>
        </div>

        <section class="page tabs">
            <div class="container">

                <ul class="materials-nav">
                    <li class="active"><a href="#" data-target=".tab1">Этажи роста</a></li>
                    <li><a href="#" data-target=".tab2">Новости</a></li>
                    <li><a href="#" data-target=".tab3">авторские материалы</a></li>
                </ul>

                <div class="rows">
                    <div class="col-left">
                        <div class="page-body content">

                            <div class="tabs-content mb-30">

                                <div class="tabs-item tab1 active">

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <a class="btn-view" href="#">Показать все</a>

                                </div>

                                <div class="tabs-item tab2">

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <a class="btn-view" href="#">Показать все</a>

                                </div>

                                <div class="tabs-item tab3">

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <div class="news-block user-news news-block-white">
                                        <div class="news-block-image">
                                            <div class="image-wrap">
                                                <div class="image-inner">
                                                    <img src="images/news_image3.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="news-block-content">
                                            <div class="hews-block-tags">29 окт. 2017</div>
                                            <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a></div>
                                            <div class="news-block-text">Роль:  Участник</div>
                                        </div>
                                    </div>

                                    <a class="btn-view" href="#">Показать все</a>

                                </div>

                            </div>

                            <div class="block-long mb-30">
                                <div class="block-long-row">
                                    <div class="block-long-left">
                                        <div class="h1">Чего можешь достичь?</div>
                                        <div class="text">Вступи в МИЦ и узнай!</div>
                                    </div>
                                    <div class="block-long-right">
                                        <a href="#" class="btn"><i class="fa fa-eye"></i> Узнать подробнее</a>
                                    </div>
                                </div>
                            </div>

                            <div class="h2 heading-arrow">Карточка контактов</div>

                            <div class="row">

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                <a class="tel" href="tel:+79656965303"><i class="fa fa-phone"></i> <span>+7 (965) 696-53-03</span></a>
                                                <br>
                                                <a class="email" href="mailto:fomina@ulmic.ru"><i class="fa fa-envelope"></i> <span>fomina@ulmic.ru</span></a>
                                            </div>
                                            <div class="social-list">
                                                <a href="#"><i class="fa fa-vk"></i></a>
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-instagram"></i></a>
                                                <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                                <a href="#"><i class="fa fa-youtube"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                <a class="tel" href="tel:+79656965303"><i class="fa fa-phone"></i> <span>+7 (965) 696-53-03</span></a>
                                                <br>
                                                <a class="email" href="mailto:fomina@ulmic.ru"><i class="fa fa-envelope"></i> <span>fomina@ulmic.ru</span></a>
                                            </div>
                                            <div class="social-list">
                                                <a href="#"><i class="fa fa-vk"></i></a>
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-instagram"></i></a>
                                                <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                                <a href="#"><i class="fa fa-youtube"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-right">

                        <div class="bnr">
                            <img src="images/banner.jpg" class="img-responsive" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>

        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                        center: [55.76, 37.64],
                        zoom: 10
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    // Создание макета содержимого хинта.
                    // Макет создается через фабрику макетов с помощью текстового шаблона.
                    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
                        "<b>{{ properties.object }}</b><br />" +
                        "{{ properties.address }}" +
                        "</div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );

                var myPlacemark = new ymaps.Placemark([55.764286, 37.581408], {
                    address: "Москва, ул. Зоологическая, 13, стр. 2",
                    object: "Центр современного искусства"
                }, {
                    hintLayout: HintLayout
                });

                myMap.geoObjects.add(myPlacemark);
            }

        </script>

    </body>
</html>

