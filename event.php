<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>

        <section class="page">


            <div class="container">
                <ul class="breadcrumbs">
                    <li><a href="#">Лидерство</a></li>
                    <li>Я лидер</li>
                </ul>

                <div class="top-box">
                    <div class="top-box-image">
                        <img src="img/img_box_01.jpg" class="img-responsive" alt="">
                    </div>
                    <div class="top-box-content">
                        <div class="top-box-heading">настало Время быть лидером</div>
                        <ul class="top-box-list">
                            <li><span>29 октября 2017 в 12:00 - 15:00</span></li>
                            <li><span>Чердаклы</span></li>
                            <li><span>Организатор: Ассоциация начинающих журналистов «Inформат» </span></li>
                        </ul>
                        <div class="top-box-action">
                            <a href="#" class="btn"><i class="fa fa-paper-plane-o"></i> Участвовать</a>
                            <div>
                                <a href="#" class="btn-text">Написать организатору</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="rows">
                    <div class="col-left">
                        <div class="page-body content">

                            <div class="h2 heading-arrow">спикеры:</div>

                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="contact-card">
                                        <div class="contact-card-image">
                                            <div class="card-image-inner">
                                                <img src="images/card_image.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                        <div class="contact-card-content">
                                            <div class="contact-card-header">
                                                <div class="contact-name">Константин Ильин</div>
                                                <div class="contact-staff">Председатель</div>
                                            </div>
                                            <div class="contact-source">
                                                Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <a class="btn-view" href="#">больше новостей</a>

                            <br/>
                            <br/>

                            <p>"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские навыки, познакомиться с новыми людьми и узнать то, что раньше было покрыто занавесом тайн.</p>
                            29 октября вас ждет насыщенная программа:
                            <ul>
                                <li>Тренинг "Лидер и его команда"</li>
                                <li>Деловая игра</li>
                                <li>Масса нужных лафхаков при работе с командой</li>
                            </ul>
                            <p>Ждем к себе в гости активных, амбициозных и любопытных! Если ты хочешь развить лидерские качества и научиться работать в команде, то тебе к нам! По всем вопросам обращайтесь к https://ulmic.ru/671 (Виктории Тарасовой) Следи за новостями на нашей страничке: https://vk.com/event154326865</p>

                            <div class="post-cast">
                                <div class="post-cast-social">
                                    <span>Рассказать друзьям:</span>
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>

                            <div class="subscribe-block">
                                <div class="subscribe-icon">
                                    <img src="img/icon_email.png" alt="" class="img-responsive">
                                </div>
                                <div class="subscribe-text">Подписаться на рассылку о мероприятиях МИЦ</div>
                                <div class="subscribe-btn">
                                    <a href="#" class="btn"><i class="fa fa-paper-plane"></i>Подписаться на рассылку</a>
                                </div>
                            </div>

                            <div class="materials">
                                <h4>Дополнительные материалы:</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#">Мы открываем новую площадку, на которой молодежь</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="#">Мы открываем новую площадку, на которой молодежь</a>
                                    </div>
                                </div>
                            </div>

                            <div class="promo">
                                <div class="promo-text">
                                    <div class="promo-text">
                                        <span>Хотите стать<br>членом МИЦ?</span>
                                        <strong>Вступайте<br>прямо сейчас</strong>
                                    </div>
                                    <a href="#" class="btn-idea">
                                        <svg class="ico-svg" viewBox="0 0 360 60" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite.svg#icon-bg" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                        <span><i class="fa fa-paper-plane"></i>Отправить заявку</span>
                                    </a>
                                </div>
                            </div>

                            <h2 class="heading-arrow">Фотоотчёт с места событий</h2>

                            <div class="slider">

                                <div class="slider-main">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                    <img src="images/slide_02.jpg" class="img-responsive">
                                    <img src="images/slide_01.jpg" class="img-responsive">
                                    <img src="images/slide_03.jpg" class="img-responsive">
                                </div>

                                <div class="slider-thumbs">
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_02.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_01.jpg" class="img-responsive"></div>
                                    <div class="slider-thumbs-item"><img src="images/slide_03.jpg" class="img-responsive"></div>
                                </div>

                            </div>

                            <div class="slider-author mb-30">
                                <div class="author-photo">
                                    <img src="images/author_photo.png" class="img-responsive" alt="">
                                </div>
                                <div class="author-text">
                                    Фотограф:
                                    <strong>Иванов Иван Алексеевич</strong>
                                </div>
                                <div class="social-list">
                                    <a href="#"><i class="fa fa-vk"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-odnoklassniki"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>

                            <h2 class="heading-arrow">Участники:</h2>
                            <div class="people-block">
                                <div class="row">

                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-6 col-lg-4">
                                        <div class="people">
                                            <div class="people-image">
                                                <div class="people-image-item">
                                                    <img src="images/card_image.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                            <div class="people-name">Константин <br/>Ильин</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-right">

                        <div class="side-box side-border">
                            <div class="h3 heading-arrow">Внимание! <br/>Новая информация!</div>
                            <div class="side-text">Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские навыки, познакомиться с новыми людьми и узнать то, что раньше было покрыто занавесом тайн.</div>
                            <div class="side-text">Мы открываем новую площадку, на которой молодежь Чердаклов может прокачать в себе лидерские навыки, познакомиться с новыми людьми и узнать то, что раньше было покрыто занавесом тайн.</div>
                        </div>

                        <div class="side-box box-gray">
                            <div class="h3 heading-arrow">Похожие мероприятия</div>
                            <div class="side-event">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                                <div class="side-event-heading"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                <div class="side-event-date">12 октября 2017</div>
                                <a href="#" class="btn" tabindex="0"><i class="fa fa-eye"></i> Узнать подробнее</a>
                            </div>
                        </div>

                        <div class="side-box box-white">
                            <div class="h3 heading-arrow">новости по программе</div>
                            <ul class="side-news">
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Экспедиция МИЦ будет длиться 27 дней</a></div>
                                </li>
                                <li>
                                    <div class="side-news-date">12 октября 2017</div>
                                    <div class="side-news-name"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <div class="events-block">
            <div class="container">

                <div class="events-heading">
                    <div class="h2 heading-arrow">Интересные мероприятия по теме: Лидерство</div>
                </div>

                <ul class="events-row">
                    <li>
                        <div class="event-item">
                            <div class="event-image">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                                <div class="event-status"><span>лидер</span></div>
                            </div>
                            <div class="event-content">
                                <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event-item">
                            <div class="event-image">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                                <div class="event-status"><span>лидер</span></div>
                            </div>
                            <div class="event-content">
                                <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event-item">
                            <div class="event-image">
                                <img src="images/news_image.jpg" class="img-responsive" alt="">
                                <div class="event-status"><span>лидер</span></div>
                            </div>
                            <div class="event-content">
                                <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="text-center">
                    <a href="#" class="btn"><i class="fa fa-refresh"></i> Показать больше мероприятий</a>
                </div>

            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>

        <script>

            ymaps.ready(init);

            function init () {
                var myMap = new ymaps.Map("map", {
                        center: [55.76, 37.64],
                        zoom: 10
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    // Создание макета содержимого хинта.
                    // Макет создается через фабрику макетов с помощью текстового шаблона.
                    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
                        "<b>{{ properties.object }}</b><br />" +
                        "{{ properties.address }}" +
                        "</div>", {
                            // Определяем метод getShape, который
                            // будет возвращать размеры макета хинта.
                            // Это необходимо для того, чтобы хинт автоматически
                            // сдвигал позицию при выходе за пределы карты.
                            getShape: function () {
                                var el = this.getElement(),
                                    result = null;
                                if (el) {
                                    var firstChild = el.firstChild;
                                    result = new ymaps.shape.Rectangle(
                                        new ymaps.geometry.pixel.Rectangle([
                                            [0, 0],
                                            [firstChild.offsetWidth, firstChild.offsetHeight]
                                        ])
                                    );
                                }
                                return result;
                            }
                        }
                    );

                var myPlacemark = new ymaps.Placemark([55.764286, 37.581408], {
                    address: "Москва, ул. Зоологическая, 13, стр. 2",
                    object: "Центр современного искусства"
                }, {
                    hintLayout: HintLayout
                });

                myMap.geoObjects.add(myPlacemark);
            }

        </script>

    </body>
</html>

