<!doctype html>
<html class="no-js" lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">


        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body>

        <header class="header">
            <div class="container">
                <a class="header-logo" href="/">
                    <img src="img/logo.png" class="img-responsive">
                </a>
                <div class="header-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="header-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="header-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
                <div class="header-login">
                    <div class="header-profile">
                        <div class="header-profile-image">
                            <div class="image-inner">
                                <img src="images/user_photo.jpg" class="img-responsive">
                            </div>
                            <span>3</span>
                        </div>
                        <ul class="header-profile-text">
                            <li><span>Наталья Чернета</span></li>
                            <li><a href="#">Выйти</a></li>
                            <li><a href="#">Мой профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <nav class="topnav">
            <div class="container">
                <div class="topnav-header">
                    <a href="#" class="header-search-btn"></a>
                    <span class="header-nav-btn navbar-toggle" data-target=".navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </div>

                <div class="navbar">
                    <a href="#" class="navbar-search"></a>
                    <ul class="navbar-nav">
                        <li><a href="#"><span>Кто мы такие?</span></a></li>
                        <li><a href="#">С чего начать?</a></li>
                        <li>
                            <a href="#">Вступить в МИЦ</a>
                            <ul class="dropnav">
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                                <li><a href="#">Пункт меню</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span>Как открыть отделение?</span></a></li>
                        <li><a href="#"><span>Партнёрам</span></a></li>
                        <li><a href="#"><span>Контакты</span></a></li>
                    </ul>
                </div>

                <div class="top-search">
                    <form class="form">
                        <input class="form-search" type="text" name="search" placeholder="Поиск по сайту...">
                        <button class="btn-search" type="submit"></button>
                    </form>
                </div>

            </div>
        </nav>


        <section class="jumbotron">
            <div class="container">
                <div class="jumbotron-slider">

                    <div class="jumbotron-item">
                        <div class="jumbotron-item-inner">
                            <a href="#">
                                <div class="jumbotron-image">
                                    <img src="images/jumbotron_01.jpg" class="img-responsive">
                                </div>
                                <div class="jumbotron-content">
                                    <div class="jumbotron-date">12 октября 2017 </div>
                                    <div class="jumbotron-text">В Ульяновске соберут представителей Клубов молодых инициаторов со всего региона</div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="jumbotron-item">
                        <div class="jumbotron-item-inner">
                            <a href="#">
                                <div class="jumbotron-image">
                                    <img src="images/jumbotron_02.jpg" class="img-responsive">
                                </div>
                                <div class="jumbotron-content">
                                    <div class="jumbotron-date">12 октября 2017 </div>
                                    <div class="jumbotron-text">В Ульяновске соберут представителей Клубов молодых инициаторов со всего региона</div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="jumbotron-item">
                        <div class="jumbotron-item-inner">
                            <a href="#">
                                <div class="jumbotron-image">
                                    <img src="images/jumbotron_01.jpg" class="img-responsive">
                                </div>
                                <div class="jumbotron-content">
                                    <div class="jumbotron-date">12 октября 2017 </div>
                                    <div class="jumbotron-text">В Ульяновске соберут представителей Клубов молодых инициаторов со всего региона</div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="jumbotron-item">
                        <div class="jumbotron-item-inner">
                            <a href="#">
                                <div class="jumbotron-image">
                                    <img src="images/jumbotron_02.jpg" class="img-responsive">
                                </div>
                                <div class="jumbotron-content">
                                    <div class="jumbotron-date">12 октября 2017 </div>
                                    <div class="jumbotron-text">В Ульяновске соберут представителей Клубов молодых инициаторов со всего региона</div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <div class="block-one">
            <div class="container">
                <div class="col-md-7 col-lg-7">
                    <div class="news-container">

                        <div class="h1 heading-arrow">Новости</div>

                        <div class="filter-tags">
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>Гражданская активность</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>Журналистика</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>IT</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>Право</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>Добровольчество</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>НКО</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'">
                                    <span>Корпоративные проекты</span>
                                </label>
                            </div>
                            <div class="tags-item">
                                <label>
                                    <input type="checkbox" value="'" checked="">
                                    <span>Все новости</span>
                                </label>
                            </div>

                        </div>

                        <div class="news-list">
                            <div class="news-list-item image-enable">
                                <div class="news-list-image">
                                    <img src="images/news_image2.jpg" class="img-responsive">
                                </div>
                                <div class="news-list-content">
                                    <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                    <div class="news-date">12 октября 2017 </div>
                                    <div class="news-text">На нем собралось свыше 100 школьников и студентов, мечтающих связать будущее со сферой информационных...</div>
                                </div>
                            </div>

                            <div class="news-list-item">
                                <div class="news-list-content">
                                    <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                    <div class="news-date">12 октября 2017 </div>
                                </div>
                            </div>

                            <div class="news-list-item">
                                <div class="news-list-content">
                                    <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                    <div class="news-date">12 октября 2017 </div>
                                </div>
                            </div>

                            <div class="news-list-item">
                                <div class="news-list-content">
                                    <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                    <div class="news-date">12 октября 2017 </div>
                                </div>
                            </div>

                        </div>

                        <a class="btn-view" href="#">больше новостей</a>

                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="nav-container">
                        <div class="nav-block">
                            <div class="h3 heading-arrow">Молодежный инициативный центр</div>
                            <ul>
                                <li><a href="#">Программы</a></li>
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">Отделения</a></li>
                                <li><a href="#">Наша команда</a></li>
                                <li><a href="#">Медиа</a></li>
                                <li><a href="#">Вступить в МИЦ</a></li>
                                <li><a href="#">Окна</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>

                        <div class="side-info">
                            <div class="side-info-border">
                                <div class="side-info-wrap">
                                    <div class="side-info-date">3-7 ноября, 2017</div>
                                    <div class="side-info-title">Ассамблея активной молодежи</div>
                                    <div class="side-info-action">
                                        <a class="side-order" href="#">Прием заявок</a>
                                        <a class="side-link" href="#">aam2017.tilda.ws</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="events-block">
            <div class="container">

                <div class="events-heading">
                    <div class="h1 heading-arrow">Мероприятия</div>
                    <ul class="city-list">
                        <li class="city-item"><a href="#">Ульяновск</a></li>
                        <li class="city-item"><a href="#">Димитровград</a></li>
                        <li class="city-item active"><a href="#">Новоульяновск</a></li>
                        <li class="city-item"><a href="#">Базарный Сызган</a></li>
                        <li class="city-item"><a href="#">Радищево</a></li>
                        <li><a href="#" class="city-dropdown-header"><i class="fa fa-angle-down"></i></a></li>
                    </ul>
                </div>

                <div class="tabs">

                    <div class="tabs-nav">
                        <ul class="tabs-nav events-nav">
                            <li class="active"><a href="#" data-target=".tab1">Гражданская активность</a></li>
                            <li><a href="#" data-target=".tab2">Журналистика</a></li>
                            <li><a href="#" data-target=".tab3">IT</a></li>
                            <li><a href="#" data-target=".tab4">Право</a></li>
                            <li><a href="#" data-target=".tab5">Добровольчество</a></li>
                            <li><a href="#" data-target=".tab6">НКО</a></li>
                            <li><a href="#" data-target=".tab7">Корпоративные проекты</a></li>
                            <li class="active"><a href="#" data-target=".tab8">Все</a></li>
                        </ul>
                    </div>

                    <ul class="events-row">
                        <li>
                            <div class="event-item">
                                <div class="event-image">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                    <div class="event-status"><span>лидер</span></div>
                                </div>
                                <div class="event-content">
                                    <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                    <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                    <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="event-item">
                                <div class="event-image">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                    <div class="event-status"><span>лидер</span></div>
                                </div>
                                <div class="event-content">
                                    <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                    <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                    <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="event-item">
                                <div class="event-image">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                    <div class="event-status"><span>лидер</span></div>
                                </div>
                                <div class="event-content">
                                    <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                    <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                    <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="event-item">
                                <div class="event-image">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                    <div class="event-status"><span>лидер</span></div>
                                </div>
                                <div class="event-content">
                                    <div class="event-date">29 окт. 2017 12:00 - 15:00</div>
                                    <div class="event-heading"><a href="#">Разум человека может добиться всего, что представит!</a></div>
                                    <div class="event-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую площадку, на которой молодежь...</div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="text-center">
                        <a href="#" class="btn"><i class="fa fa-refresh"></i> Показать больше мероприятий</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="figure figure-yellow">
            <div class="container">
                <div class="figure-row">
                    <div class="figure-heading">
                        <span>Цифра дня</span>
                        <strong>1039 книг</strong>
                    </div>
                    <div class="figure-content"><i>Помогли собрать активисты МИЦ Минестерству культуры для создания книжных точек в больницах области.</i></div>
                </div>
            </div>
        </div>

        <section class="operation">
            <div class="container">
                <div class="h1 heading-arrow">направления деятельности</div>

                <ul class="operation-accordion">

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">Право</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">Лидерство</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item open">
                        <div class="operation-header">
                            <div class="operation-header-text">Волантерство</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                                                <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">Журналистика</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">ИT</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">Корпоративные проекты</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="operation-item">
                        <div class="operation-header">
                            <div class="operation-header-text">НКО</div>
                            <span class="operation-dropdown"></span>
                        </div>
                        <div class="operation-content">

                            <div class="content-image-left">
                                <img src="images/content_image_01.jpg" class="img-responsive" alt="">
                            </div>

                            <div class="heading-h2">О проекте:</div>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями Проект #мечтайсомной будет рад вашему участию в таком важном, интересном и полезном деле как реализация заветных мечтаний детей с опасными для жизни заболеваниями.</p>
                            <p>Любая благотворительность не может развиваться без помощи людей. Есть профессионалы, которые занимаются этим каждый день, в том числе помогают и нашему проекту. Но только массовое участие неравнодушных людей делает любую мечту ребенка способной осуществиться.</p>
                            <a href="#" class="btn-view">Перейти на страницу программы</a>

                            <div class="clearfix mb-30"></div>

                            <div class="row">
                                <div class="col-md-6 col-lg-7">
                                    <div class="heading-h2">Ближайшие мероприятия:</div>

                                    <div class="news-list">
                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Ассоциация начинающих журналистов Ульяновской области «Inформат» (формирует реестр «Малые медиа Ульяновской области».</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                        <div class="news-list-item">
                                            <div class="news-list-content">
                                                <div class="news-title"><a href="#">Первый 24часовой литературный хакатон прошел в Ульяновске</a></div>
                                                <div class="news-date">12 октября 2017 </div>
                                            </div>
                                        </div>

                                    </div>

                                    <a class="btn-view" href="#">Все мероприятия</a>

                                </div>
                                <div class="col-md-6 col-lg-5">
                                    <div class="block-high block-transparent">
                                        <div class="block-high-inner">
                                            <div class="h1">У вас есть идея для мероприятия?</div>
                                            <div class="block-text">Оставьте нам заявку и мы выслушаем ваши предложения!</div>
                                            <a href="#" class="btn"><i class="fa fa-paper-plane"></i> Предложить мероприятие</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>

            </div>
        </section>

        <div class="figure figure-green">
            <div class="container">
                <div class="figure-row">
                    <div class="figure-heading">
                        <span>Цифра дня</span>
                        <strong>от 14 лет.</strong>
                    </div>
                    <div class="figure-content"><i>Вступить в организацию могут люди от 14 лет. Ежегодно в наших мероприятиях принимают участие свыше 7 000 чел.</i></div>
                </div>
            </div>
        </div>

        <section class="idea-block">
            <div class="container">
                <div class="idea-content">
                    <div class="idea-heading">
                        <div class="heading-one">Есть <i><strong>идея</strong></i> для <br/>мероприятия</div>
                        <div class="heading-two">Хотите создать<br/><i><strong>свой проект?</strong></i></div>
                    </div>
                    <p>Расскажите о нем нам и мы поможем вам воплотить его в жизнь!</p>
                    <a href="#" class="btn-idea">
                        <svg class="ico-svg" viewBox="0 0 360 60" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite.svg#icon-bg" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                        <span><i class="fa fa-paper-plane"></i>Отправить заявку</span>
                    </a>
                </div>
            </div>
        </section>

        <section class="press">
            <div class="container">
                <div class="press-heading">
                    <div class="h1 heading-arrow">СМИ о нас</div>
                    <a href="#" class="for_press">Для сми</a>
                </div>
                <div class="press-slider">

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_01.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_02.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_01.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_02.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_01.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="press-slider-item">
                        <div class="press-content">
                            <div class="press-image">
                                <img src="images/press_02.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="press-text">
                                <div class="press-date">29 окт. 2017 12:00 - 15:00</div>
                                <h5>Разум человека может добиться всего, что представит!</h5>
                                <p>"Время быть лидером" новый проект от Чердаклинского отделения.
                                    Мы открываем новую площадку, на которой молодежь...</p>
                                <div class="press-footer">
                                    <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="library">
            <div class="container">
                <div class="h1 heading-arrow">Библиотека OKNA.ulmic.ru</div>
                <div class="tabs">
                    <div class="tabs-nav">
                        <ul class="tabs-nav library-nav">
                            <li class="active"><a href="#" data-target=".tab1">Гражданская активность</a></li>
                            <li><a href="#" data-target=".tab2">Журналистика</a></li>
                            <li><a href="#" data-target=".tab3">IT</a></li>
                            <li><a href="#" data-target=".tab4">Право</a></li>
                            <li><a href="#" data-target=".tab5">Добровольчество</a></li>
                            <li><a href="#" data-target=".tab6">НКО</a></li>
                            <li><a href="#" data-target=".tab7">Корпоративные проекты</a></li>
                            <li><a href="#" data-target=".tab8">Все</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8">
                            <div class="tabs-content">

                                <div class="news-block news-block-white">
                                    <div class="news-block-image">
                                        <div class="image-wrap">
                                            <div class="image-inner">
                                                <img src="images/news_image_lg.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="news-block-content">
                                        <div class="hews-block-tags">29 окт. 2017 12:00 - 15:00  |  Автор: <a href="#">Марина кузнецова</a></div>
                                        <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a> / <a href="#" class="news-article">Право</a></div>
                                        <div class="news-block-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую.</div>
                                        <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                    </div>
                                </div>

                                <div class="news-block news-block-white">
                                    <div class="news-block-image">
                                        <div class="image-wrap">
                                            <div class="image-inner">
                                                <img src="images/news_image_lg.jpg" class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="news-block-content">
                                        <div class="hews-block-tags">29 окт. 2017 12:00 - 15:00  |  Автор: <a href="#">Марина кузнецова</a></div>
                                        <div class="news-block-heading"><a class="news-block-title" href="#">Разум человека может добиться всего, что представит!</a> / <a href="#" class="news-article">Право</a></div>
                                        <div class="news-block-text">"Время быть лидером" новый проект от Чердаклинского отделения. Мы открываем новую.</div>
                                        <a href="#" class="btn"><i class="fa fa-eye"></i> Читать полностью</a>
                                    </div>
                                </div>

                            </div>
                            <a class="btn-view" href="#">больше на сайте okna.ulmic.ru</a>
                        </div>
                        <div class="col-md-4 col-lg-4 hidden-xs hidden-sm">
                            <div class="bnr">
                                <img src="images/banner.jpg" class="img-responsive" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="figure figure-blue">
            <div class="container">
                <div class="figure-row">
                    <div class="figure-heading">
                        <span>Цифра дня</span>
                        <strong>7000 чел.</strong>
                    </div>
                    <div class="figure-content"><i>МИЦ – это молодая и амбициозная команда профессионалов своего дела, которая объединилась, чтобы реализовать свои идеи и поддерживать инициативы других.</i></div>
                </div>
            </div>
        </div>

        <section class="command">
            <div class="command-header">
                <div class="container">
                    <div class="h1 heading-arrow">Я в команде МИЦ</div>
                </div>
            </div>
            <ul class="command-block">
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_01.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_02.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_03.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_04.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_05.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_06.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_07.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_08.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_09.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#" class="command-item">
                        <img src="images/command/command_10.jpg" class="img-responsive">
                        <div class="command-text">
                            <div class="command-text-wrap">
                                <div class="command-user">Светлана Мосалёва</div>
                                <p>Не вне, а в нас самих и бед и счастья семя № 115</p>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </section>

        <footer class="footer">
            <div class="container">
                <a href="/" class="footer-logo">
                    <img src="img/footer_logo.png" class="img-responsive" alt="">
                </a>
                <ul class="footer-nav">
                    <li><a href="#">Кто мы такие?</a></li>
                    <li><a href="#">Вступить в МИЦ</a></li>
                    <li><a href="#">Партнёрам</a></li>
                    <li><a href="#">С чего начать?</a></li>
                    <li><a href="#">Как открыть отделение?</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>

                <div class="footer-contact">
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    <a class="footer-tel" href="tel:+7 (976) 771-44-10">+7 (976) <strong>771-44-10</strong></a>
                    <p><a class="footer-email" href="mailto:activ@ulmic.ru">activ@ulmic.ru</a></p>
                </div>
            </div>
        </footer>

        <a class="btn-up" href="#header"></a>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/jquery.selectric/jquery.selectric.js"></script>
        <script src="js/vendor/air-datepicker/datepicker.min.js"></script>
        <script src="js/vendor/jquery.scrollTo.min.js"></script>
        <script src="js/vendor/slick/slick.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>

